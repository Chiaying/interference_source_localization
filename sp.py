#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: signal_probe
# Author: chiaying
# GNU Radio version: v3.10-compat-xxx-xunknown

from gnuradio import blocks
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
import time




class sp(gr.top_block):

    def __init__(self, samp_rate=10e6, gain=20, center_freq=3555e6, lo_offset=5e6, index=0, start_time=59, duration = 2):
        gr.top_block.__init__(self, "signal_probe")

        ##################################################
        # Parameters
        ##################################################
        self.samp_rate = samp_rate
        self.gain = gain
        self.center_freq = center_freq
        self.lo_offset = lo_offset
        self.index = index
        self.start_time = start_time
        self.duration = duration
        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_source_0 = uhd.usrp_source(
            ",".join(("", "")),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
        )
        self.uhd_usrp_source_0.set_clock_source('external', 0)
        self.uhd_usrp_source_0.set_time_source('external', 0)
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0.set_time_unknown_pps(uhd.time_spec(0))
        
        tr = uhd.tune_request(center_freq, lo_offset)
        #tr.rf_freq = lo_offset        
        tr.rf_freq_policy = uhd.tune_request.POLICY_MANUAL
        #tr.dsp_freq = 0
        tr.dsp_freq_policy = uhd.tune_request.POLICY_AUTO
        
        #self.uhd_usrp_source_0.set_center_freq(uhd.tune_request(center_freq,lo_offset),0)
        self.uhd_usrp_source_0.set_center_freq(tr)
        self.uhd_usrp_source_0.set_antenna('TX/RX', 0)
        self.uhd_usrp_source_0.set_gain(gain, 0)
        self.blocks_head_0 = blocks.head(gr.sizeof_gr_complex*1, int(duration*samp_rate))
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, 'disturb', False)
        self.blocks_file_sink_0.set_unbuffered(False)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_head_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.blocks_head_0, 0))

        self.init_timed_streaming(self.start_time)
        
    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.uhd_usrp_source_0.set_gain(self.gain, 0)

    def get_center_freq(self):
        return self.center_freq
    
    def get_lo_offset(self):
        return self.lo_offset
    
    def set_center_freq(self, center_freq, lo_offset):
        self.center_freq = center_freq
        self.lo_offset = lo_offset
        self.uhd_usrp_source_0.set_center_freq(uhd.tune_request(self.center_freq,self.lo_offset),0)

    def get_index(self):
        return self.index

    def set_index(self, index):
        self.index = index

    def init_timed_streaming(self, start_time):
        user_start_time = (int(start_time),)

        # Convert local time to sturct_time format
        local_time = time.time()
        user_time = time.localtime(local_time)

        # Create future time in struct_time format
        t = user_time[0:4]+user_start_time+(0,)+user_time[6:9]

        # Convert future time to seconds
        future_time = time.mktime(t)
        print('Start time in Sec: ', future_time)

        local_time = time.time()
        start = int(future_time-local_time)

        self.uhd_usrp_source_0.set_start_time(uhd.time_spec(start))

        self.uhd_usrp_source_0.set_time_unknown_pps(uhd.time_spec(0))
        curr_hw_time = self.uhd_usrp_source_0.get_time_last_pps()
        while curr_hw_time==self.uhd_usrp_source_0.get_time_last_pps():
                pass
        time.sleep(0.05)

        self.uhd_usrp_source_0.set_time_next_pps(uhd.time_spec_t(0))

        time.sleep(2)

def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-s", "--samp-rate", dest="samp_rate", type=eng_float, default=eng_notation.num_to_str(float(10e6)),
        help="Set samp_rate [default=%(default)r]")
    parser.add_argument(
        "-g", "--gain", dest="gain", type=eng_float, default=eng_notation.num_to_str(float(20)),
        help="Set gain [default=%(default)r]")
    parser.add_argument(
        "-f", "--center-freq", dest="center_freq", type=eng_float, default=eng_notation.num_to_str(float(3555e6)),
        help="Set center_freq [default=%(default)r]")
    parser.add_argument(
        "-lo", "--lo-offset", dest="lo_offset", type=eng_float, default=eng_notation.num_to_str(float(10e6)),
        help="Set lo_offset [default=%(default)r]")
    parser.add_argument(
        "-i", "--index", dest="index", type=eng_float, default=eng_notation.num_to_str(float(0)),
        help="Set index [default=%(default)r]")
    parser.add_argument(
        "-t", "--start_time", dest="start_time", type=eng_float, default="59.0",
        help="Set start time in min relative to curr hour [default=%(default)r]")
    parser.add_argument(
        "-d", "--duration", dest="duration", type=eng_float, default="2",
        help="Set receiver duration in sec [default=%(default)r]")
        
    return parser


def main(top_block_cls=sp, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(samp_rate=options.samp_rate, gain=options.gain, center_freq=options.center_freq, lo_offset=options.lo_offset, index=options.index, start_time=options.start_time, duration=options.duration)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    tb.wait()


if __name__ == '__main__':
    main()
