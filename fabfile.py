#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import time
import datetime
import multiprocessing as mp
#from fabric import Connection, Config
from fabric import *
from fabric2 import Connection
from fabric.api import *
import subprocess
from fabric.api import env
env.use_ssh_config = True
env.password = 'shivon' 

RX = "RX"


def parse_file(filename):
    """
    Parses the specified text file containing the transmitter / receiver nodes in the following format:
        NAME | ROLE | SSH ADDRESS
        -----------------
        "HOSTNAME" | RX  | ssh [-p portnum] [user@]<address>
        -----------------
    @filename: path to the hostfile

    Returns a dictionary of { hostname : addr }
    """
    node_dict = {}

    file = open(filename, "r")

    for line in file:
        name, role, ssh = line.split(' | ')
        _, _, port, host = ssh.split(' ')
        node_dict[name] = host.rstrip() + ":" + port

    return node_dict


@hosts("localhost")
def grab_cir_measurements(rx_node_dict):
    """
    Transfers CIR files from each of the rx hosts and renames them to HOSTNAME_CIR in the cwd
    @rx_node_dict: dictionary of receiver nodes in form { hostname : addr }
    @curr_tx_name: the hostname of the current tx
    """
    for key in rx_node_dict:
        name, host = key, rx_node_dict[key]
        print("Downloading raw file form host %s" % name)
        result = Connection(host,connect_kwargs={"password":"shivon"}).get("disturb")
        print("Downloaded {0.remote} to {0.local}")
        local("mv disturb rx_%s_raw" % name)


@hosts('localhost')
def set_hosts():
    """
    Helper function to set the env.hosts variable based on parsed file

    Returns a dictionary of { hostname : addr }
    """
    host_dict= parse_file("hostsfile.txt")

    print("host dict: ", host_dict)

    env.hosts = list(host_dict.values())
    hostnames = list(host_dict.keys())

    print("env.hosts: ", env.hosts)

    return host_dict


#########################
### RX FUNCTIONS ###
#########################

@roles('rx')
@parallel
def rx_function():
    """
    runs the receiver process on the rx host
    """
    def get_next_min():
        now = datetime.datetime.now().minute
        next_min = (now + 1)

        return next_min

    # function to start transmitting at specified minute of hour
    def rx(rx_min):
        run("sp.py -f 3580e6 -s 200e6 -lo 100e6 -g 27 -d 0.1 -t %d" % rx_min)

    next_min = get_next_min()  # grab next minute

    rx(next_min)  # run sp.py at next minute


###########################
### CONTAINER FUNCTIONS ###
###########################

@roles('rx')
def rx_start():
    execute(rx_function)


#################
### MAIN TASK ###
#################

def main():
    """
    The main entrypoint for our fabfile.
    Sets the env.hosts list conduct sp experiments.
    """
    host_dict = set_hosts()
    print("env.hosts: ", env.hosts)

    rx_hosts = env.hosts.copy()
    
    print("rx_hosts: ", rx_hosts)
    env.roledefs.update({
        'rx': rx_hosts
    })    
    
    rx_hosts_dict = host_dict.copy()
    rx_start()  # starts processes all receivers in parallel
 
    # transfers the IQ sample files from remote to local
    grab_cir_measurements(rx_hosts_dict)

